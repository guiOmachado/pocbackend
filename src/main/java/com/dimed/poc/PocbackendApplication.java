package com.dimed.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocbackendApplication.class, args);
	}

}
