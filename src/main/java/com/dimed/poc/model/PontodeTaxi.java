package com.dimed.poc.model;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "pontostaxi")
public class PontodeTaxi {

	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "latitude")
	private double latitude;

	@Column(name = "longitude")
	private double longitude;

	@Column(name = "criado_em")
	private Date criadoEm;

	public PontodeTaxi(String nome, double latitude, double longitude) {
		super();
		this.nome = nome;
		this.latitude = latitude;
		this.longitude = longitude;
		this.criadoEm = new Date();
	}

	public PontodeTaxi() {

	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public Date getHora() {
		return criadoEm;
	}

	public void setHora() {
		this.criadoEm = new Date();
	}
}
