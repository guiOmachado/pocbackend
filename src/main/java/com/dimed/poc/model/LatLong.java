package com.dimed.poc.model;

public class LatLong {
	
	private double lat;
	private double lng;

	public LatLong() {
		// TODO Auto-generated constructor stub
	}	

	public LatLong(double latitude, double longitude) {
		super();
		this.lat = latitude;
		this.lng = longitude;
	}

	public double getLatitude() {
		return lat;
	}

	public void setLatitude(double latitude) {
		this.lat = latitude;
	}

	public double getLongitude() {
		return lng;
	}

	public void setLongitude(double longitude) {
		this.lng = longitude;
	}
	
	

}
