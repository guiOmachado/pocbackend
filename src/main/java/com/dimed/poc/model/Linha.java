package com.dimed.poc.model;
import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="linhas")
public class Linha implements Serializable {	
	
	private static final long serialVersionUID = 1L;	
	
	@Id
	@Column(name="id")
	private Integer id;	
	
	@Column(name="idlinha")
	private int idlinha;
	
	@Column(name="codigo")
	private String codigo;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="latitude",columnDefinition = "LONGTEXT")
	private String latitude;
	
	@Column(name="longitude",columnDefinition = "LONGTEXT")
	private String longitude;
	
	public Linha() {
		// TODO Auto-generated constructor stub
	}		

	public Linha(int id,int idlinha, String codigo, String nome,String longitude,String latitude) {
		super();
		this.id = idlinha;
		this.idlinha = idlinha;
		this.codigo = codigo;
		this.nome = nome;
		this.latitude=latitude;
		this.longitude=longitude;
	}



	public int getId() {
		return idlinha;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdlinha() {
		return idlinha;
	}

	public void setIdlinha(int idlinha) {
		this.idlinha = idlinha;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	

}


