package com.dimed.poc.dto;

import java.util.Collection;
import java.util.List;

import com.dimed.poc.model.LatLong;
import com.dimed.poc.model.Linha;

public class LinhaRs {
	
	
	private int id;	
	private String codigo;	
	private String nome;
	private List<LatLong> latlong;

	public LinhaRs(){
		// TODO Auto-generated constructor stub
	}		

	public LinhaRs(int id, String codigo, String nome) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.nome = nome;
	}
	
	public LinhaRs(int id, String codigo, String nome,List<LatLong> latlong) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.nome = nome;
		this.latlong=latlong;
	}



	public static Linha converter(LinhaRs elemento){
		Linha linha = new Linha();
		
		linha.setId(elemento.getId());
		linha.setIdlinha(elemento.getId());
		linha.setNome(elemento.getNome());
		linha.setCodigo(elemento.getCodigo());
		
		
		return linha;
	}
	
	public static LinhaRs converter(Linha elemento){
		LinhaRs linhars = new LinhaRs();
		
		linhars.setId(elemento.getId());
		linhars.setNome(elemento.getNome());
		linhars.setCodigo(elemento.getCodigo());
		
		
		return linhars;
	}	

	public List<LatLong> getLatlong() {
		return latlong;
	}

	public void setLatlong(List<LatLong> latlong) {
		this.latlong = latlong;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	

	
}


