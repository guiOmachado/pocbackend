package com.dimed.poc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dimed.poc.model.PontodeTaxi;
import com.dimed.poc.repository.TaxiRepository;

@Service
public class TaxiServiceImpl implements TaxiService {

	@Autowired
	TaxiRepository taxiRepository;

	@Override
	public void save(PontodeTaxi p) {
		this.taxiRepository.save(p);
	}

	@Override
	public List<PontodeTaxi> findAll() {
		return this.taxiRepository.findAll();
	}

}
