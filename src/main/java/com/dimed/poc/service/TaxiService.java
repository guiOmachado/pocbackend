package com.dimed.poc.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.dimed.poc.model.Linha;
import com.dimed.poc.model.PontodeTaxi;

public interface TaxiService {

	void save(PontodeTaxi p);

	List<PontodeTaxi> findAll();

}
