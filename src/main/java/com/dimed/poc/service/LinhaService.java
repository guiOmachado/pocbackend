package com.dimed.poc.service;

import java.util.Collection;
import java.util.Optional;

import com.dimed.poc.model.Linha;

public interface LinhaService {
	
	Linha findById(Integer id);

	Collection<Linha> findAll();

	void save(Linha salvarLinha);

	Linha findLineById(Integer id);	

}
