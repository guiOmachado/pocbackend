package com.dimed.poc.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dimed.poc.model.Linha;
import com.dimed.poc.repository.LinhaRepository;

@Service
public class LinhaServiceImpl implements LinhaService {
	
	@Autowired
	LinhaRepository linhaRepository;	
		
	public Collection<Linha> findAll() {
		return this.linhaRepository.findAll();
	}
	
	public void save(Linha linha)
	{
		this.linhaRepository.save(linha);
	}

	@Override
	public Linha findLineById(Integer id) {
		   return this.linhaRepository.findLineById(id);		 
	}

	@Override
	public Linha findById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
}
