package com.dimed.poc.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import com.dimed.poc.model.PontodeTaxi;

public class ArquivoTaxi {

	public static void leitor() throws IOException {
		String caminho = ".\\pontosTaxi.txt";

		File pontos = new File(caminho);

		if (!pontos.exists()) {
			pontos.createNewFile();
		}
		BufferedReader buffRead = new BufferedReader(new FileReader(caminho));
		String linha = "";
		while (true) {
			if (linha != null) {
				System.out.println(linha);

			} else
				break;
			linha = buffRead.readLine();
		}
		buffRead.close();
	}

	public static void escritor(PontodeTaxi p) throws IOException {
		String caminho = ".\\pontosTaxi.txt";

		File pontos = new File(caminho);

		if (!pontos.exists()) {
			pontos.createNewFile();
		}

		BufferedWriter buffWrite = new BufferedWriter(new FileWriter(caminho,true));

		String linha = p.getNome() + "#" + p.getLatitude() + "#" + p.getLongitude() + "#" + p.getHora();		

		buffWrite.append(linha + "\n");
		buffWrite.close();
	}

}
