package com.dimed.poc.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dimed.poc.model.PontodeTaxi;
import com.dimed.poc.service.TaxiService;
import com.dimed.poc.util.ArquivoTaxi;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "TaxiRestController")
public class TaxiRestController {

	@Autowired
	private TaxiService taxiService;

	@ApiOperation(value = "Adiciona um ponto de taxi pegando por parametro seu nome, latitude e longitude")
	@RequestMapping(value = "/addpontotaxi", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<PontodeTaxi> adicionaPontodeTaxi(@RequestParam(name = "nome") String nome,
			@RequestParam(name = "latitude") double latitude, @RequestParam(name = "longitude") double longitude)
			throws IOException {

		ArquivoTaxi manipulaArquivo = new ArquivoTaxi();
		PontodeTaxi p = new PontodeTaxi(nome, latitude, longitude);

		manipulaArquivo.escritor(p);

		this.taxiService.save(p);

		manipulaArquivo.leitor();

		return ResponseEntity.ok(p);
	}

	@ApiOperation(value = "Retorna todos os pontos de taxi cadastrados")
	@RequestMapping(value = "/getpontosdetaxi", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<PontodeTaxi>> findPontodeTaxi() throws IOException {

		ArquivoTaxi manipulaArquivo = new ArquivoTaxi();
		List<PontodeTaxi> p = new ArrayList<PontodeTaxi>();

		p = this.taxiService.findAll();

		return ResponseEntity.ok(p);
	}
}
