package com.dimed.poc.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.dimed.poc.dto.LinhaFiltradaRs;
import com.dimed.poc.dto.LinhaRs;
import com.dimed.poc.model.LatLong;
import com.dimed.poc.model.Linha;
import com.dimed.poc.model.PontodeTaxi;
import com.dimed.poc.repository.LinhaRepository;
import com.dimed.poc.service.LinhaService;
import com.dimed.poc.service.TaxiService;
import com.dimed.poc.util.ArquivoTaxi;
import com.dimed.poc.util.CalculaDistancia;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "TransporteRestController")
public class TransporteRestController {

	@Autowired
	private LinhaService linhaservice;
	


	@ApiOperation(value = "Faz o mapeamento das linhas e etinerarios salvando no banco de dados")
	@RequestMapping(value = "/getlines", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<LinhaRs>> getLinhas() throws InterruptedException {

		List<LinhaRs> linhars = new ArrayList<LinhaRs>();
		List<Linha> linhas = new ArrayList<Linha>();

		RestTemplate template = new RestTemplate();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
		template.getMessageConverters().add(converter);

		ResponseEntity<List<LinhaRs>> entity = template.exchange(
				"http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<LinhaRs>>() {
				});

		linhars = entity.getBody();

		for (LinhaRs elemento : linhars) {
			linhas.add(elemento.converter(elemento));
		}

		for (Linha salvarLinha : linhas) {
			this.linhaservice.save(salvarLinha);
			this.getItinerarios(salvarLinha.getId());
			Thread.sleep(100);
		}

		return ResponseEntity.ok(entity.getBody());
	}

	@ApiOperation(value = "Faz o do etiner�rio de uma linho procurando pelo ID e havendo alguma altera��o ela � salva")
	@RequestMapping(value = "/getitinerario/{idlinha}", method = RequestMethod.GET, produces = "application/json")
	public LinhaRs getItinerarios(@PathVariable Integer idlinha) {
		RestTemplate template = new RestTemplate();
		List<LatLong> latlong = new ArrayList();

		String latitude = "";
		String longitude = "";

		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
		template.getMessageConverters().add(converter);

		ResponseEntity<String> entity = template.getForEntity(
				"http://www.poatransporte.com.br/php/facades/process.php?a=il&p=" + idlinha, String.class);

		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = null;

		try {
			root = mapper.readTree(entity.getBody());
			int idPosicao = 0;

			while (true) {
				JsonNode posicao = root.path("" + idPosicao);
				if (posicao.isMissingNode()) {
					System.out.println(posicao.isMissingNode() + " " + idPosicao);
					break;
				}
				
				JsonNode lat = posicao.path("lat");
				JsonNode lng = posicao.path("lng");

				longitude += lng.asText() + ",";
				latitude += lat.asText() + ",";
				idPosicao++;
			}

		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		JsonNode name = root.path("nome");
		JsonNode idLinha = root.path("idlinha");
		JsonNode codigo = root.path("codigo");

		Linha linha = this.linhaservice.findLineById(idLinha.asInt());

		linha.setLatitude(latitude);
		linha.setLongitude(longitude);

		this.linhaservice.save(linha);

		String[] latitudeSplit = linha.getLatitude().split(",");
		String[] longitudeSplit = linha.getLongitude().split(",");

		for (int i = 0; i < longitudeSplit.length; i++) {
			latlong.add(new LatLong(Double.parseDouble(latitudeSplit[i]), Double.parseDouble(longitudeSplit[i])));
		}

		LinhaRs linhars = LinhaRs.converter(linha);

		linhars.setLatlong(latlong);

		return linhars;
	}

	@ApiOperation(value = "Procura pelas linhas dentro de um raio pegando por parametro o raio desejado, latitude e longitude que esta")
	@RequestMapping(value = "/getlinesporraio",method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<LinhaFiltradaRs>> getLinhasPorRaio(@RequestParam(name="raio") double raio,@RequestParam(name="latitude") double latitude,@RequestParam(name="longitude") double longitude) {
		
		List<Linha> linhas = (List)this.linhaservice.findAll();
		List<LinhaFiltradaRs> linhasFiltradas = new ArrayList();		
		List<LinhaRs> linhasrs = new ArrayList() ;
		CalculaDistancia filtroPorRaio = new CalculaDistancia();
		int index=0;
		while(index<linhas.size())
		{
			List<LatLong> latlong = new ArrayList();
			String[] latitudeSplit = linhas.get(index).getLatitude().split(",");
			String[] longitudeSplit = linhas.get(index).getLongitude().split(",");
	
			for (int i = 0; i < longitudeSplit.length; i++) {
				latlong.add(new LatLong(Double.parseDouble(latitudeSplit[i]), Double.parseDouble(longitudeSplit[i])));
			}
	
			LinhaRs linhars = LinhaRs.converter(linhas.get(index));
	
			linhars.setLatlong(latlong);
				
			linhasrs.add(linhars);
			
			index++;
		}
		index=0;
		
		for(LinhaRs verificaLinha: linhasrs)
		{
			while(index<verificaLinha.getLatlong().size())
			{
				double distancia = filtroPorRaio.distance(latitude, verificaLinha.getLatlong().get(index).getLatitude(), longitude, verificaLinha.getLatlong().get(index).getLongitude());
								
				if(raio>=distancia)
				{					
					System.out.println(verificaLinha.getNome()+" "+verificaLinha.getLatlong().get(index).getLatitude()+" "+verificaLinha.getLatlong().get(index).getLongitude()+" "+distancia);
					
					linhasFiltradas.add(new LinhaFiltradaRs(verificaLinha.getId(),verificaLinha.getNome(),verificaLinha.getCodigo()));
					break;
				}
			
				index++;
			}
			
		}		
		
		return ResponseEntity.ok(linhasFiltradas);
	}

	@ApiOperation(value = "Faz o mapeamento das linhas pelo nome da linha")
	@RequestMapping(value = "/getlinespornome/{name}",method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Linha>> getLinhasPorNome(@PathVariable String name) {
		RestTemplate template = new RestTemplate();
		List<Linha> linhas = new ArrayList();

		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
		converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
		template.getMessageConverters().add(converter);

		ResponseEntity<List<Linha>> entity = template.exchange(
				"http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o", HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Linha>>() {
				});

		for (Linha linha : entity.getBody()) {
			if (linha.getNome().contains(name.toUpperCase())) {
				linhas.add(linha);
			}
		}

		return ResponseEntity.ok(linhas);
	}
	

	
}
