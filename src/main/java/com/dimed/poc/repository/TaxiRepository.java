package com.dimed.poc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.dimed.poc.model.PontodeTaxi;

@Repository
public interface TaxiRepository extends JpaRepository<PontodeTaxi, Integer> {

}
