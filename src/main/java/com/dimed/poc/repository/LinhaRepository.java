package com.dimed.poc.repository;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.dimed.poc.model.Linha;

@Repository
public interface LinhaRepository extends JpaRepository<Linha, Integer> {

	@Query(value = "SELECT * FROM linhas l WHERE l.id = ?1")
    Linha findLineById(Integer id) throws DataAccessException;


}
