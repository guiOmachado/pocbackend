FROM openjdk:15-jdk-alpine

ARG PROFILE
ARG ADDITIONAL_OPTS

ENV PROFILE=${PROFILE}
ENV ADDITIONAL_OPTS=${ADDITIONAL_OPTS}

WORKDIR /opt/spring_boot

COPY /build/libs/pocbackend-0.0.1-SNAPSHOT.jar pocbackend-0.0.1-SNAPSHOT.jar

SHELL ["/bin/sh", "-c"]

EXPOSE 5005
EXPOSE 8080

CMD java ${ADDITIONAL_OPTS} -jar pocbackend-0.0.1-SNAPSHOT.jar --spring.profiles.active=${PROFILE}
