# pocbackend

pocbackend dimed

Comandos para execução do projeto:
- gradle build -x test  | usei esse comando pois no build estava dando conflito com o mysql local e do docker
- docker-compose up --build --force-recreate

técnologias:
- springBoot 
- mysql -> o uso deste banco de dados se ao fato de ja estar familharizado e assim conseguir me dedicar mais 
            ao aprendizado do springBoot para a construção das apis.


Exemplos de request:
- http://localhost:8080/getlines
- http://localhost:8080/getpontosdetaxi
- http://localhost:8080/addpontotaxi?nome=taxi dimed&latitude=-31.078736495726965&longitude=-51.11564576199
- http://localhost:8080/getlinesporraio/?raio=11&latitude=-30.078736495726965&longitude=-51.115645761998714
- http://localhost:8080/getitinerario/5520
- http://localhost:8080/getbyid/5516
- http://localhost:8080/getlinespornome/santana
